import "../scss/app.scss";
import {pluck} from 'ramda';

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const arrayToPluck = [
    { name: "John", class: "is-primary" },
    { age: 23, class: "is-warning" },
    { job: "programmer", class: "is-danger" },
  ];
  const articles = document.querySelectorAll("article");
  var pluckClasses = R.pluck('class');
  pluckClasses([{class: 'is-primary'}, {class: 'is-warning'}, {class: 'is-danger'}]);
});
